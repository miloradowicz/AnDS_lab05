﻿using System;
using System.Collections.Generic;

namespace MyDataTypes
{
  public interface IBinarySearchTree<T>
    where T : IComparable
  {
    string Listing { get; }
    IEnumerable<T> PreorderTraverse { get; }
    IEnumerable<T> InorderTraverse { get; }
    IEnumerable<T> PostorderTraverse { get; }

    bool Search(T v);

    bool Add(T v);

    void Delete(T v);

    void Clear();
  }

  public class AVLTree<T> : IBinarySearchTree<T>
    where T : IComparable
  {
    private class Node
    {
      internal T _value;
      internal int _bf;
      internal Node _left;
      internal Node _right;

      internal Node(T v)
      {
        _value = v;
        _bf = 0;
        _left = null;
        _right = null;
      }
    }

    private Node _root;
    private int _count;

    public AVLTree()
    {
      _root = null;
      _count = 0;
    }

    private int GetBalanceFactor(Node n)
    {
      return (n._right != null ? GetSubtreeHeight(n._right) : 0) - (n._left != null ? GetSubtreeHeight(n._left) : 0);
    }

    private int GetSubtreeHeight(Node n)
    {
      return Math.Max(n._left != null ? GetSubtreeHeight(n._left) + 1 : 0, n._right != null ? GetSubtreeHeight(n._right) + 1 : 0);
    }

    private int GetNodeDepth(Node n)
    {
      int d = 0;
      for (Node t = _root; t != n;)
      {
        d++;
        if (n._value.CompareTo(t._value) < 0)
          t = t._left;
        else
          t = t._right;
      }

      return d;
    }

    private Node RotateLeft(Node n)
    {
      Node t;
      //if (n._right == null)
      //  return n;
      //else
      t = n._right;

      n._right = t._left;
      t._left = n;

      if (t._bf == 0)
      {
        n._bf = +1;
        t._bf = -1;
      }
      else
      {
        n._bf = 0;
        t._bf = 0;
      }

      return t;
    }

    private Node RotateRightLeft(Node n)
    {
      Node t, u;
      //if (n._left == null || n._left._right == null)
      //{
      //  return n;
      //}
      //else
      {
        t = n._right;
        u = t._left;
      }

      n._right = u._left;
      t._left = u._right;
      u._left = n;
      u._right = t;

      if (u._bf == 0)
      {
        n._bf = 0;
        t._bf = 0;
      }
      else if (u._bf < 0)
      {
        n._bf = 0;
        t._bf = +1;
      }
      else
      {
        n._bf = -1;
        t._bf = 0;
      }
      u._bf = 0;

      return u;
    }

    private Node RotateRight(Node n)
    {
      Node t;
      //if (n._left == null)
      //  return n;
      //else
      t = n._left;

      n._left = t._right;
      t._right = n;

      if (t._bf == 0)
      {
        n._bf = -1;
        t._bf = +1;
      }
      else
      {
        n._bf = 0;
        t._bf = 0;
      }

      return t;
    }

    private Node RotateLeftRight(Node n)
    {
      Node t, u;
      //if (n._right == null || n._right._left == null)
      //{
      //  return n;
      //}
      //else
      {
        t = n._left;
        u = t._right;
      }

      n._left = u._right;
      t._right = u._left;
      u._left = t;
      u._right = n;

      if (u._bf == 0)
      {
        n._bf = 0;
        t._bf = 0;
      }
      else if (u._bf < 0)
      {
        n._bf = +1;
        t._bf = 0;
      }
      else
      {
        n._bf = 0;
        t._bf = -1;
      }
      u._bf = 0;

      return u;
    }

    public T Top
    {
      get { return _root._value; }
    }

    public string Listing
    {
      get
      {
        string body = "";

        Stack<Node> stack = new Stack<Node>();
        if (_root != null)
          stack.Push(_root);

        while (stack.Count != 0)
        {
          Node p = stack.Pop();
          if (p._left != null)
          {
            stack.Push(p._left);
            body += $"{p._value} -> {p._left._value} [color=red]\n";
          }
          if (p._right != null)
          {
            stack.Push(p._right);
            body += $"{p._value} -> {p._right._value} [color=blue]\n";
          }
        }

        return "digraph {\n" + body + "}";
      }
    }

    public IEnumerable<T> PreorderTraverse
    {
      get
      {
        Stack<Node> stack = new Stack<Node>();
        if (_root != null)
          stack.Push(_root);

        while (stack.Count != 0)
        {
          Node p = stack.Pop();
          if (p._right != null)
            stack.Push(p._right);
          if (p._left != null)
            stack.Push(p._left);

          yield return p._value;
        }
      }
    }

    public IEnumerable<T> InorderTraverse
    {
      get
      {
        Stack<Node> stack = new Stack<Node>();
        Action<Node> pushleft = (Node n) =>
        {
          for (Node t = n; t != null;)
          {
            stack.Push(t);
            t = t._left;
          }
        };

        pushleft(_root);

        while (stack.Count != 0)
        {
          Node p = stack.Pop();
          pushleft(p._right);

          yield return p._value;
        }
      }
    }

    public IEnumerable<T> PostorderTraverse
    {
      get
      {
        Stack<Node> stack = new Stack<Node>();
        Node q = _root;

        if (_root != null)
          stack.Push(_root);

        while (stack.Count != 0)
        {
          Node p = stack.Peek();
          if (p._left == null && p._right == null || p._left == q || p._right == q)
          {
            q = p;
            stack.Pop();
            yield return p._value;
          }
          else
          {
            if (p._right != null)
              stack.Push(p._right);
            if (p._left != null)
              stack.Push(p._left);
          }
        }
      }
    }

    public bool Search(T v)
    {
      for (Node t = _root; t != null;)
      {
        if (v.CompareTo(t._value) == 0)
          return true;
        else if (v.CompareTo(t._value) < 0)
          t = t._left;
        else
          t = t._right;
      }

      return false;
    }

    public bool Add(T v)
    {
      Stack<Node> stack = new Stack<Node>();
      for (Node t = _root; t != null;)
      {
        if (v.CompareTo(t._value) == 0)
        {
          return false;
        }
        else
        {
          stack.Push(t);
          if (v.CompareTo(t._value) < 0)
            t = t._left;
          else
            t = t._right;
        }
      }

      if (stack.Count == 0)
      {
        _root = new Node(v);
      }
      else
      {
        Node p = stack.Peek();
        Node w = new Node(v);
        if (v.CompareTo(p._value) < 0)
          p._left = w;
        else
          p._right = w;

        while (stack.Count != 0)
        {
          Node q;
          p = stack.Pop();
          if (p._left == w)
          {
            if (p._bf < 0)
            {
              if (w._bf > 0)
                q = RotateLeftRight(p);
              else
                q = RotateRight(p);
            }
            else if (p._bf > 0)
            {
              p._bf = 0;

              break;
            }
            else
            {
              p._bf = -1;
              w = p;

              continue;
            }
          }
          else
          {
            if (p._bf > 0)
            {
              if (w._bf < 0)
                q = RotateRightLeft(p);
              else
                q = RotateLeft(p);
            }
            else if (p._bf < 0)
            {
              p._bf = 0;

              break;
            }
            else
            {
              p._bf = +1;
              w = p;

              continue;
            }
          }

          if (stack.Count == 0)
          {
            _root = q;
          }
          else
          {
            Node r = stack.Peek();
            if (r._left == p)
              r._left = q;
            else
              r._right = q;
          }

          break;
        }
      }

      return true;
    }

    public void Delete(T v)
    {
      Stack<Node> stack = new Stack<Node>();
      bool found = false;
      for (Node t = _root; t != null && !found;)
      {
        stack.Push(t);
        if (v.CompareTo(t._value) == 0)
          found = true;
        else if (v.CompareTo(t._value) < 0)
          t = t._left;
        else
          t = t._right;
      }

      if (!found)
        return;

      int altered = 0;
      int b;
      Node u = stack.Pop();
      Node p, q, w;
      if (u._right == null || u._left == null)
      {
        Node t = u._right != null ? u._right : u._left;
        if (stack.Count == 0)
        {
          _root = t;

          return;
        }
        else
        {
          Node r = stack.Peek();
          if (r._left == u)
          {
            r._left = t;
          }
          else
          {
            r._right = t;
          }
          w = t;
        }
      }
      else
      {
        stack.Push(u);
        for (Node t = u._right; t != null;)
        {
          stack.Push(t);
          t = t._left;
        }

        w = stack.Pop();
        p = stack.Peek();
        u._value = w._value;
        if (p == u)
        {
          p._right = w._right;
        }
        else
        {
          p._left = w._right;
        }
        w = w._right;
      }

      while (stack.Count != 0)
      {
        p = stack.Pop();
        if (p._left == w)
        {
          if (p._bf > 0)
          {
            b = p._right._bf;
            if (b < 0)
              q = RotateRightLeft(p);
            else
              q = RotateLeft(p);
          }
          else if (p._bf < 0)
          {
            p._bf = 0;
            w = p;

            continue;
          }
          else
          {
            p._bf = +1;

            break;
          }
        }
        else
        {
          if (p._bf < 0)
          {
            b = p._left._bf;
            if (b > 0)
              q = RotateLeftRight(p);
            else
              q = RotateRight(p);
          }
          else if (p._bf > 0)
          {
            p._bf = 0;
            w = p;

            continue;
          }
          else
          {
            p._bf = -1;

            break;
          }
        }

        if (stack.Count == 0)
        {
          _root = q;
        }
        else
        {
          Node r = stack.Peek();
          if (r._left == p)
            r._left = q;
          else
            r._right = q;
        }

        if (b == 0)
          break;
        else
          w = q;
      }
    }

    public void Clear()
    {
      _root = null;
    }
  }
}