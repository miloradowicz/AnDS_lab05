﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using MyDataTypes;

namespace AnDS_lab5
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      Console.Title = "-=Binary search tree=-";

      AVLTree<int> tree = new AVLTree<int>();

      bool exit = false;
      while (!exit)
      {
        Console.Clear();
        Console.WriteLine("Choose action:");
        Console.WriteLine("  1 - Fill");
        Console.WriteLine("  2 - Search");
        Console.WriteLine("  3 - Add");
        Console.WriteLine("  4 - Delete");
        Console.WriteLine("  5 - Traverse");
        Console.WriteLine("  6 - Visualize");
        Console.WriteLine("  8 - Quit");
        Console.WriteLine();
        switch (Console.ReadKey(true).KeyChar)
        {
          case '1':
            {
              tree.Clear();
              Console.Write("Enter the number of values: ");
              int num = Convert.ToInt32(Console.ReadLine());
              Console.WriteLine("Choose fill method:");
              Console.WriteLine("  1 - Fill automatically");
              Console.WriteLine("  2 - Fill manually");
              Console.WriteLine("  3 - Fill with arithmetic progression");
              bool success = false;
              while (!success)
              {
                switch (Console.ReadKey(true).KeyChar)
                {
                  case '1':
                    Random rnd = new Random();
                    for (int i = 0; i < num;)
                    {
                      if (tree.Add(rnd.Next(0, num << 4)))
                        i++;
                    }
                    success = true;
                    break;

                  case '2':
                    for (int i = 0; i < num;)
                    {
                      Console.Write("Enter the value: ");
                      int input = Convert.ToInt32(Console.ReadLine());
                      if (tree.Add(input))
                        i++;
                      else
                        Console.WriteLine("The value already exists");
                    }
                    success = true;
                    break;

                  case '3':
                    for (int i = 0; i < num; i++)
                      tree.Add(i);
                    success = true;
                    break;

                  default:
                    success = false;
                    break;
                }
              }
            }
            break;

          case '2':
            {
              Console.Write("Enter the value to search: ");
              int input = Convert.ToInt32(Console.ReadLine());
              if (tree.Search(input))
                Console.WriteLine("  {0} is found", input);
              else
                Console.WriteLine("  {0} is not found", input);
              Console.WriteLine("Press any key to continue");
              Console.ReadKey(true);
            }
            break;

          case '3':
            {
              Console.Write("Enter the value to add: ");
              int input = Convert.ToInt32(Console.ReadLine());
              if (tree.Add(input))
                Console.WriteLine("  {0} was added", input);
              else
                Console.WriteLine("  {0} already exists", input);
              Console.WriteLine("Press any key to continue");
              Console.ReadKey(true);
            }
            break;

          case '4':
            {
              Console.Write("Enter the value to delete: ");
              int input = Convert.ToInt32(Console.ReadLine());
              tree.Delete(input);
              Console.WriteLine("Press any key to continue");
              Console.ReadKey(true);
            }
            break;

          case '5':
            {
              Console.WriteLine("Choose traverse method:");
              Console.WriteLine("  1 - Preorder traverse");
              Console.WriteLine("  2 - Inorder traverse");
              Console.WriteLine("  3 - Postorder traverse");
              bool success = false;
              while (!success)
              {
                switch (Console.ReadKey(true).KeyChar)
                {
                  case '1':
                    foreach (int i in tree.PreorderTraverse)
                      Console.WriteLine(i);
                    success = true;
                    break;

                  case '2':
                    foreach (int i in tree.InorderTraverse)
                      Console.WriteLine(i);
                    success = true;
                    break;

                  case '3':
                    foreach (int i in tree.PostorderTraverse)
                      Console.WriteLine(i);
                    success = true;
                    break;

                  default:
                    success = false;
                    break;
                }
              }
              Console.WriteLine("Press any key to continue");
              Console.ReadKey(true);
            }
            break;

          case '6':
            {
              using (StreamWriter sw = new StreamWriter("output.dot"))
              {
                sw.Write(tree.Listing);
                sw.Close();
                Process dot = new Process();
                dot.StartInfo = new ProcessStartInfo(".\\external\\dot.exe");
                dot.StartInfo.Arguments = "-Tpng .\\output.dot -o .\\output.png";
                dot.StartInfo.CreateNoWindow = true;
                dot.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                dot.Start();
                dot.WaitForExit();
                Process png = new Process();
                png.StartInfo = new ProcessStartInfo(".\\output.png");
                png.StartInfo.UseShellExecute = true;
                png.Start();
              }
            }
            break;

          case '7':
            {
              tree.Delete(tree.Top);
            }
            break;

          case '8':
            exit = true;
            break;

          default:
            break;
        }
      }
    }
  }
}